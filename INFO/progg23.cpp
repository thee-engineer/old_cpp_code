/*
Se citeste un numar natural N de la tastatura, nenul si maxim 9 cifre
sa se verifice daca numarul contine cifre consecutive aflate in orice ordine
*/
#include <iostream>
#include <string.h>
#include <conio.h>

using namespace std;

char b[10];
char c;

int main (){

for (int i = 0 ; i < 9 ; i++){
    b[i] = getch();
    cout << b[i];

	if (b[i] == '\n')
		break;
}

for (int j = 0 ; j < 9 ; j++)
for (int k = 0 ; k < 9 ; k++){

    if (b[k] >= b[k+1]){

        c = b[k+1];
        b[k+1] = b[k];
        b[k] = c;
    }
    else
    {
        c = b[k];
        b[k] = b[k+1];
        b[k+1] = c;
    }
}

cout << endl << b;

return 0;}
