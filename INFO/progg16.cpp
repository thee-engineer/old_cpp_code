/*
pentru 2 siruri de caractere cu maxim 250 fiecare (litere mici)
cu caracterele in ordinea alfabetica, introduse de la tastarura

se cere sa se afiseze de la ecran un al treilea sir format din toate char primelor 2 siruri
asezate in ordine alfabetica

alegeti un algo eficient din punct de vedere al timpului de executare

    a) descrie strategia de rezolvare si justficati eficienta algo ales folosind limbajul natural (5-6 randuri)
    b) scrieti prog corespunzator metodei descrise

    in      |   out
            |
   ampstz   |   abfgmopssttxz
   bfgostx  |
            |
*/

#include <iostream>
#include <string.h>

using namespace std;

char s1[251],s2[251];
int l1,l2;
//int fl;

int i,k;

int main (){

cin >> s1;l1 = strlen(s1);
cin >> s2;l2 = strlen(s2);

//if (l1 >= l2) fl = l1; else fl = l2;

while (i < l1 && k < l2){

    if (s1[i] < s2[k]){

        cout << s1[i];
        i++;
        }
    else{

        cout << s2[k];
        k++;
        }

//if (i < l1){
//    cout << s1 +i;
//}
//else if (k < l2)
//{
//    cout << s2 +k;
//}


}

return 16;}
