#include <string>
#include <iostream>
using namespace std;

void task1(string msg)
{
    cout << "task1 says: " << msg;
}

int main()
{
    thread t1(task1, "Hello");
    t1.join();
}
