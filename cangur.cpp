/* Saritura cangurului

Un cangur a sarit in prima zi 7m.
In a doua zi a sarit in plus fata de ziua precedenta
de 10 ori mai mult (70m).
In a treia zi a sarit in plus fata de prima zi de 10 ori mai mult
decat ziua a doua (700m)
In a patra zi a sarit in plus fata de prima zi de zece ori mai mult decat in ziua
a treia. (7000m).
Si asa mai departe

Scrieti un program care calculeaza cati metri a sarit cangurulin total
in n zile.
*/
#include <iostream>
#include <windows.h>

using namespace std;

int main()
{

int i,z;

cout << "Numarul de zile in care cangurul sare:";
cin  >> z;

do
{
cout <<"7";
z=z-1;
}
while (z!=0);

loop:

cout << endl << "Numarul de zile in care cangurul sare:";
cin  >> z;

for (i = 1; i <= z; z--)

cout << 7;

goto loop;

return 0;
}
