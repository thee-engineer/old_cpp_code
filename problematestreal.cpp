#include <iostream>

using namespace std;

int main()
{

    int a;

    cin >> a;

    cout << a%10<<endl; //CORECT

    cout << a%100<<endl;

    cout << a%100+10<<endl;

    cout << a%100-a%10<<endl;

    cout << (a%100-a%10)/10<<endl; //CORECT

    cout << a/10<<endl;

    return 0;
}
